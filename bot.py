from discord import channel
from discord.ext import commands, tasks
import os
from dotenv import load_dotenv
from itertools import cycle
import logging
import json

from pycoingecko import CoinGeckoAPI
from icecream import ic

load_dotenv()

GUILD_ID = 310875169398718465

NOTIFICATIONS_CHANNEL_NAME = 'notifications'
NOTIFICATIONS_CHANNEL_ID = 885939681026506763

CURRENT_TRADES_CHANNEL_NAME = 'current-trades'
CURRENT_TRADES_CHANNEL_ID = 885938674905272330

TRADE_ANALYSIS_CHANNEL_NAME = 'trade-notifications'
TRADE_ANALYSIS_CHANNEL_ID = 884099626192494632

# This is the role that can use the commands
ALLOWED_ROLE_ID = 754153266819891241

client = commands.Bot(command_prefix = '!')

## TIMED LOOPS
# background functions
@tasks.loop(seconds=5)
async def Check_Prices():
    cg = CoinGeckoAPI()
    
    # get json info from file
    price_list_json = GetJsonFile()


    full_names_string = ''
    #get all full_names and put them in an string to pass to coingecko api
    i = 0
    for c in price_list_json:
        i += 1
        if i != len(price_list_json):
            full_names_string += price_list_json[c]['full_name'] + ','
        else:
            full_names_string += price_list_json[c]['full_name']

    # check prices with alert prices from price_list.json - all_coin_prices is now a dictionary of prices from coingecko
    all_coin_prices = cg.get_price(ids=full_names_string, vs_currencies=price_list_json[c]['currency'])
    output_channel = client.get_channel(TRADE_ANALYSIS_CHANNEL_ID)
    prices_list = []
    for p in all_coin_prices:
        temp_value = all_coin_prices[p].values()
        value_interator = iter(temp_value)
        coin_price = float(next(value_interator))
        prices_list.append(coin_price)

    # as the function says, it gets the entries and adds the full_name and abbreviation
    entries = GetEntries(price_list_json)

    i = 2
    for e in entries:
        for p in e:
            print(p)
        i += 1

@tasks.loop(seconds=5)
async def Update_Current_Trades():
    current_trades_channel = client.get_channel(CURRENT_TRADES_CHANNEL_ID)

    price_list_json = GetJsonFile()


    output_string = ''
    emoji_list = ['🟢', '🔴']

    # Loop through the json file and get each coin
    for coin in price_list_json:
        coin_trade_type = price_list_json[coin]['trade_type']
        coin_symbol = price_list_json[coin]['symbol']
        coin_currency = price_list_json[coin]['currency']
        coin_stop_loss = price_list_json[coin]['stop_loss']
        coin_take_profit = price_list_json[coin]['take_profit']
        coin_reward_ratio = price_list_json[coin]['reward_ratio']
        coin_entry_list = price_list_json[coin]['entry_list']
        entries = ''
        # checks if it is a long or short to change emoji at start
        if coin_trade_type == 'LONG':
            entry_count = 1
            i = 0
            for entry in coin_entry_list:
                if i < len(coin_entry_list)-1:
                    entries += '📍Entry ' + str(entry_count) + ': ' + coin_entry_list[entry]['value'] + '\n'
                else:
                    entries += '📍Entry ' + str(entry_count) + ': ' + coin_entry_list[entry]['value']
                entry_count += 1  
            output_string += f'%s%s ORDER %s/%s\n\n%s\n🛑SL: %s\n💰TP: %s\n⚖️RR: %s\n\n' %(emoji_list[0], coin_trade_type, coin_symbol, coin_currency, entries, coin_stop_loss, coin_take_profit, coin_reward_ratio)
        elif coin_trade_type == 'SHORT':
            entry_count = 1
            i = 0
            for entry in coin_entry_list:
                if i < len(coin_entry_list)-1:
                    entries += '📍Entry ' + str(entry_count) + ': ' + coin_entry_list[entry]['value'] + '\n'
                else:
                    entries += '📍Entry ' + str(entry_count) + ': ' + coin_entry_list[entry]['value']
                entry_count += 1  
            try:
                temp_reward_ratio = int(coin_reward_ratio)
                output_string += f'%s%s ORDER %s/%s\n\n%s\n🛑SL: %s\n💰TP: %s\n\n' %(emoji_list[1], coin_trade_type, coin_symbol, coin_currency, entries, coin_stop_loss, coin_take_profit)
            except ValueError:
                output_string += f'%s%s ORDER %s/%s\n\n%s\n🛑SL: %s\n💰TP: %s\n⚖️RR: %s\n' %(emoji_list[1], coin_trade_type, coin_symbol, coin_currency, entries, coin_stop_loss, coin_take_profit, coin_reward_ratio)

    # Grabs the message that was sent by bot IF BOT CANT GRAB LAST MESSAGE USE !reset IN THE CHAT
    if output_string != '':
        message = await current_trades_channel.fetch_message(current_trades_channel.last_message_id)
        await message.edit(content=output_string)

# Example: LONG BTC/USD Entry 1 HIT (4234.43) Trade Active
@tasks.loop(seconds=2)
async def Update_Notifications():
    notifications_channel = client.get_channel(NOTIFICATIONS_CHANNEL_ID)

    price_list_json = GetJsonFile()

    SetEntryHits(price_list_json)
    cg_hit_prices = GetPrices(price_list_json, True)
    
    output_message = ''
    coins_that_hit = ''
    # if there is a hit in the dictonary
    
    if cg_hit_prices:
        # loop through the dictionary
        for cg_coin_name in cg_hit_prices:
            # loop through price_list_json
            for json_coin in price_list_json:
                # setting output variables to read easier
                trade_type = price_list_json[json_coin]['trade_type']
                symbol = price_list_json[json_coin]['symbol']
                currency = price_list_json[json_coin]['currency']
                # if the fullname of the price list json is equal to the coin name in the cg dictonary
                if price_list_json[json_coin]['full_name'] == cg_coin_name:
                    # loop through each entry in the json coin
                    for entry in price_list_json[json_coin]['entry_list']:
                        # if the trade type of the coin is LONG
                        
                        if trade_type == 'LONG':
                            # if the price in the json entry is less than or equal to the cg price 
                            if float(price_list_json[json_coin]['entry_list'][entry]['value']) >= float(cg_hit_prices[cg_coin_name][price_list_json[json_coin]['currency'].lower()]) and price_list_json[json_coin]['entry_list'][entry]['notified'] != True:
                                
                                price_list_json[json_coin]['entry_list'][entry]['notified'] = True
                                
                                SavePriceList(price_list_json)
                                entry_number = 'Entry ' + entry.split('y')[1]
                                cg_price = float(cg_hit_prices[cg_coin_name][price_list_json[json_coin]['currency'].lower()])
                                output_message = f'%s %s/%s %s HIT (%s) Trade Active' %(trade_type, symbol, currency, entry_number, cg_price)
                                await notifications_channel.send(output_message)
                        elif trade_type == 'SHORT':
                            # if the price in the json entry is less than or equal to the cg price 
                            if float(price_list_json[json_coin]['entry_list'][entry]['value']) <= float(cg_hit_prices[cg_coin_name][price_list_json[json_coin]['currency'].lower()]) and price_list_json[json_coin]['entry_list'][entry]['notified'] != True:
                                ic('Short activated')
                                price_list_json[json_coin]['entry_list'][entry]['notified'] = True
                                
                                SavePriceList(price_list_json)
                                entry_number = 'Entry ' + entry.split('y')[1]
                                cg_price = float(cg_hit_prices[cg_coin_name][price_list_json[json_coin]['currency'].lower()])
                                output_message = f'%s %s/%s %s HIT (%s) Trade Active' %(trade_type, symbol, currency, entry_number, cg_price)
                                await notifications_channel.send(output_message)
                
      
## EVENTS
@client.event
async def on_ready():
    print('{0.user} is ready!'.format(client))
    Update_Notifications.start()
    Update_Current_Trades.start()

@client.event
async def on_message(message):
    if message.author != client.user:
        channel = message.channel
        # makes sure the message is said in this channel name
        if str(channel) == TRADE_ANALYSIS_CHANNEL_NAME:

            msg = str(message.content)
            # checks if the message is a command, if it is await for the command and retunr out the on_message
            if msg[0] == '!':
                await client.process_commands(message)
                return
            
            msg = msg.split('\n')

            msg_list = list(filter(None, msg))
            # first check to see if there is anything in the msg trade type
            
            try:
                msg_trade_type = str(msg_list[0].split(' ')[1])
            except IndexError:
                ic('msg_trade_type did not have anything in it')
                return



            trade_type_list = msg_list[0].split(' ')

            trade_type =  trade_type_list[1]

            if str(trade_type) != 'LONG' and str(trade_type) != 'SHORT':
                ic('The message was not a LONG or SHORT message.')
                return
            coin_type = trade_type_list[3].split('/')[0]
            currency = trade_type_list[3].split('/')[1]
            stop_loss = 0
            take_profit = 0
            reward_ratio = 0
            entry_list = []
            full_name = ''
            # PARSES MESSAGE INTO VARIABLES ABOVE
            i = 1
            for m in msg:
                if 'Entry' in m:
                    
                    temp_m = m.split(':')[1].replace(' ', '')
                    entry_list.append(temp_m)
                    i += 1
                elif 'SL' in m:
                    stop_loss = m.split(':')[1].replace(' ', '')
                elif 'TP' in m:
                    take_profit = m.split(':')[1].replace(' ', '')
                elif 'RR' in m:
                    reward_ratio = (m.split(':')[1]+':'+m.split(':')[2]).replace(' ', '')
            print(entry_list)
            # This area handles the logic for JSON

            price_list_json = GetJsonFile()

            add_to_json_bool = True
            duplicate_id = None

            for i in price_list_json:
                # checks the json file if the coin we are adding is a duplicate
                if str(price_list_json[i]['symbol']) == str(coin_type):
                    # DUPLICATE: Do not add to the json list
                    if price_list_json[i]['trade_type'] == trade_type:
                        add_to_json_bool = False
                        duplicate_id = int(price_list_json[i]['json_id'])
                        break
                    
            # had to put this here out the loop or else it will give false positives and negatives
            #checks if to add or edit a json entry and if true add to it else edit it
            if add_to_json_bool:
                # No duplicate so add to the json file

                coin_names = []
                for i in price_list_json:
                    coin_names.append(i)
                
                if len(coin_names) > 0:
                    last_json_obj_id = price_list_json[coin_names[len(coin_names)-1]]['json_id']
                else:
                    last_json_obj_id = 0

                data = {
                        last_json_obj_id+1: {
                            "json_id":last_json_obj_id+1,
                            "symbol": coin_type,
                            "full_name": full_name,
                            "trade_type": trade_type,
                            "currency": currency,
                            "stop_loss": stop_loss,
                            "take_profit": take_profit,
                            "reward_ratio": reward_ratio,
                            "entry_list": {}
                        }
                    }
                    # adds the entry list as a dictionary instead of a list
                for idx, val in enumerate(entry_list):
                    data[last_json_obj_id+1]['entry_list']['entry'+ str(idx+1)] = {"value":val, "hit": False, "notified": False} 
                    
                price_list_json.update(data)
                with open("price_list.json", 'w') as write_file:
                    json.dump(price_list_json, write_file)
                    write_file.close()
                ic(f'%s is not a duplicate' % coin_type)
            else:
                # There is a duplicate so we need to edit this duplicate
                data = {}

                for idx, val in enumerate(entry_list):
                    data['entry'+ str(idx+1)] = {"value":val, "hit": False, "notified": False}


                grabbed_coin_name = GrabCoinNameByJsonId(duplicate_id, price_list_json)
                
                price_list_json[grabbed_coin_name]['trade_type'] = trade_type
                price_list_json[grabbed_coin_name]['currency'] = currency
                price_list_json[grabbed_coin_name]['stop_loss'] = stop_loss
                price_list_json[grabbed_coin_name]['take_profit'] = take_profit
                price_list_json[grabbed_coin_name]['entry_list'] = data
                price_list_json[grabbed_coin_name]['reward_ratio'] = reward_ratio
                price_list_json[grabbed_coin_name]['full_name'] = full_name


                price_list_file = open('price_list.json', 'w')
                json.dump(price_list_json, price_list_file)
                price_list_file.close()
            # await channel.send(message.content)
    await client.process_commands(message)

## COMMANDS
@client.command(aliases=['delete'])
async def delete_coin(ctx, coin_type):
    # only allows a certain role to acceess the commands
    allowed = False
    for role in ctx.author.roles:
        if role.id == ALLOWED_ROLE_ID:
            allowed = True
    if str(ctx.message.channel) == TRADE_ANALYSIS_CHANNEL_NAME and allowed:

        price_list_json = GetJsonFile()
  
        coin_to_delete = None

        for i in price_list_json:
            if i == coin_type:
                coin_to_delete = coin_type

        if coin_to_delete is not None:
            price_list_json.pop(coin_to_delete, None)
        
            with open("price_list.json", 'w') as write_file:
                json.dump(price_list_json, write_file)
                write_file.close()
            await ctx.send(f'Your coin type is now deleted: %s' %coin_type)
        else:
            await ctx.send(f'%s was not listed, please check your spelling.' %coin_type)

@client.command(aliases=['list'])
async def list_coins(ctx):
    # only allows a certain role to acceess the commands
    allowed = False
    for role in ctx.author.roles:
        if role.id == ALLOWED_ROLE_ID:
            allowed = True
    if str(ctx.message.channel) == TRADE_ANALYSIS_CHANNEL_NAME and allowed:

        price_list_json = GetJsonFile()

        output = ''


        for i in price_list_json:
            entry_string = ''
            increment = 1
            for ii in price_list_json[i]['entry_list']:
                entry_string += f':round_pushpin:Entry %s: %s\n' %(increment, ii)
                increment += 1

            output+=f':green_circle: %s ORDER %s/%s\n\n%s\n:octagonal_sign: SL: %s\n:moneybag: TP: %s\n:scales: RR:%s\n\n' %(price_list_json[i]['trade_type'], i, price_list_json[i]['currency'], entry_string,price_list_json[i]['stop_loss'], price_list_json[i]['take_profit'], price_list_json[i]['reward_ratio'])
        
        await ctx.send(ctx.author.roles[1])

# Resets the notifications channel so the bot can grab last message that was his
@client.command(aliases=['reset'])
async def reset_current_trades(ctx):
    for role in ctx.author.roles:
        if role.id == ALLOWED_ROLE_ID:
            allowed = True
    if str(ctx.message.channel) == CURRENT_TRADES_CHANNEL_NAME and allowed:
        await ctx.channel.purge(limit=50)
        await ctx.channel.send('Placeholder')

@client.command(aliases=['purge'])
async def purge_channel(ctx):
    for role in ctx.author.roles:
        if role.id == ALLOWED_ROLE_ID:
            allowed = True
    
    if allowed:
        await ctx.message.channel.purge(limit=100)
    


## HELPER FUNCTIONS
# finds the coin name with a json id so we can then edit it
def GrabCoinNameByJsonId(json_id, json_list):
    coin_list = []
    # gets all the coin names
    for i in json_list:
        coin_list.append(i)
    # loop through the names and find the id and check if it is the same, if so return it
    for i in coin_list:
        if json_list[i]['json_id'] == json_id:
            return i

def GetEntries(price_list_json):
    entries = []

    #  TODO: change the append to a 3D array like this [['BTC', 'BITCOIN', '25.2', '35.8']]
    for c in price_list_json:
        i = 0
        temp_entry_str = ''
        for entry in price_list_json[c]['entry_list']:
            
            if i == len(price_list_json[c]['entry_list'])-1:
                temp_entry_str += price_list_json[c]['entry_list'][entry]['value']
            else: 
                temp_entry_str += price_list_json[c]['entry_list'][entry]['value'] + ',' 
            i += 1
        entries_temp_list = temp_entry_str.split(',')
        entries_temp_list.insert(0, price_list_json[c]['full_name'])
        entries_temp_list.insert(0, price_list_json[c]['symbol'])
        entries.append(entries_temp_list)
              
    return entries
    
def GetJsonFile():
    price_list_file = open('price_list.json', 'r')
    price_list_json = json.load(price_list_file)
    price_list_file.close()
    return price_list_json

def GetPrices(price_list_json, is_hit):
    if not is_hit:
        cg = CoinGeckoAPI()
        full_names_string = ''
        #get all full_names and put them in an string to pass to coingecko api
        i = 0
        for c in price_list_json:
            i += 1
            if i != len(price_list_json):
                full_names_string += price_list_json[c]['full_name'] + ','
            else:
                full_names_string += price_list_json[c]['full_name']

        # check prices with alert prices from price_list.json - all_coin_prices is now a dictionary of prices from coingecko
        all_coin_prices = cg.get_price(ids=full_names_string, vs_currencies=['usd'])

        return all_coin_prices
    else:
        cg = CoinGeckoAPI()
        full_names_string = ''
        #get all full_names and put them in an string to pass to coingecko api
        for c in price_list_json:
            i = 0
            for entry in list(price_list_json[c]['entry_list']):
                if price_list_json[c]['entry_list'][entry]['hit'] == True and price_list_json[c]['entry_list'][entry]['notified'] == False:
                    if i != len(price_list_json):
                        full_names_string += price_list_json[c]['full_name'] + ','
                    else:
                        full_names_string += price_list_json[c]['full_name']
                
                i += 1
        # check prices with alert prices from price_list.json - all_coin_prices is now a dictionary of prices from coingecko
        if full_names_string != '':
            all_coin_prices = cg.get_price(ids=full_names_string, vs_currencies=['usd'])
            ic(all_coin_prices)
            return all_coin_prices

def SavePriceList(price_list_json):
    with open("price_list.json", 'w') as write_file:
        json.dump(price_list_json, write_file)
        write_file.close()

def SetEntryHits(price_list_json):
    cg_prices = GetPrices(price_list_json, False)
    for cg_coin_name in cg_prices:
        # loop through price_list_json
        for json_coin in price_list_json:
            # setting output variables to read easier
            full_name = price_list_json[json_coin]['full_name']

            # if the fullname of the price list json is equal to the coin name in the cg dictonary
            if full_name == cg_coin_name:
                # loop through each entry in the json coin
                for entry in price_list_json[json_coin]['entry_list']:
                    # if the trade type of the coin is LONG
                    if price_list_json[json_coin]['trade_type'] == 'LONG':
                        # if the price in the json entry is less than or equal to the cg price 
                        if float(price_list_json[json_coin]['entry_list'][entry]['value']) >= float(cg_prices[full_name][price_list_json[json_coin]['currency'].lower()]) and price_list_json[json_coin]['entry_list'][entry]['hit'] != True:
                                price_list_json[json_coin]['entry_list'][entry]['hit'] = True
                                SavePriceList(price_list_json)
                    else:
                        # if the price in the json entry is greater than or equal to the cg price 
                        
                        if float(price_list_json[json_coin]['entry_list'][entry]['value']) <= float(cg_prices[full_name][price_list_json[json_coin]['currency'].lower()]) and price_list_json[json_coin]['entry_list'][entry]['hit'] != True:
                                price_list_json[json_coin]['entry_list'][entry]['hit'] = True
                                SavePriceList(price_list_json)
    
# need to setup python-dotenv and create this variable with the correct token DISCORD_BOT_TOKEN (pip install python-dotenv)
client.run(os.getenv('DISCORD_BOT_TOKEN'))



# :green_circle: LONG ORDER THETA/USD

# :round_pushpin:Entry 1: 6.85
# :round_pushpin:Entry 2: 6.99

# :octagonal_sign: SL: 6.48
# :moneybag: TP: 10.1
# :scales: RR: 1:8

# THETA - NAME OF coin being traded
# LONG - is the trade type, ie. Long or short (up or down)
# Sl - stop loss , the price the coin must hit for auto close the trade to avoid further loss
# Tp- take profit , the price the coin must be  to auto close ur trade and take all profit
# Rr - reward ratio , higher means better reward risk ratio








